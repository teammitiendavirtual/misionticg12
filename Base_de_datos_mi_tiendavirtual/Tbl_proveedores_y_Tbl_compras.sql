create schema if not exists Tablas_D_Ospina default character set utf8;
use Tablas_D_Ospina;
create table if not exists Tablas_D_Ospina.Proveedores(
	idProveedor int not null auto_increment,
    nombreProveedor varchar(45) not null,
    direccionProveedor varchar(45) not null,
    primary key(idProveedor)
    );

create table if not exists Tablas_D_Ospina.Compras(
	idCompra int not null auto_increment,
    fechaCompra datetime not null,
    valorCompra double not null,
    idProveedor int not null,
    primary key(idCompra),
    constraint Compras_Proveedores foreign key (idProveedor) references Proveedores (idProveedor)
    );

