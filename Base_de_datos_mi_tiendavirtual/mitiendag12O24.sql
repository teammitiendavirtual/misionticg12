SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";
CREATE DATABASE IF NOT EXISTS `mitiendag12O24` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `mitiendag12O24`;

CREATE TABLE `producto` (`idProducto` int(11) NOT NULL,
`nombreProducto` varchar(45) NOT NULL,
`valorCompra` double NOT NULL,
`valorVenta` double NOT NULL,
`cantidadProducto` double NOT NULL,
`fechaVencimiento` date NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `transaccion` (`idTransaccion` int(11) NOT NULL,
`tipoTransaccion` varchar(45) NOT NULL,
`fechaTransaccion` date NOT NULL,
`vendedor` varchar(45) NOT NULL,
`comprador` varchar(45) NOT NULL,
`total` double NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `detalle` (`idDetalle` int(11) NOT NULL,
`idProducto` int(11) NOT NULL,
`idTransaccion` int(11) NOT NULL,
`valorDetalle` double NOT NULL,
`cantidadDetalle` int NOT NULL,
`totalDetalle` double NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
ALTER TABLE `detalle`   ADD PRIMARY KEY (`idDetalle`),
ADD KEY `fk_detalle_transaccion` (`idTransaccion`),   ADD KEY `fk_detalle_producto` (`idProducto`);
ALTER TABLE `producto` ADD PRIMARY KEY (`idProducto`);  ALTER TABLE `transaccion`
ADD PRIMARY KEY (`idTransaccion`);
ALTER TABLE `detalle`   MODIFY `idDetalle` int(11) NOT NULL AUTO_INCREMENT,
AUTO_INCREMENT=4;
ALTER TABLE `producto`   MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT,
AUTO_INCREMENT=3;
ALTER TABLE `transaccion` MODIFY `idTransaccion` int(11) NOT NULL AUTO_INCREMENT,
AUTO_INCREMENT=3;
ALTER TABLE `detalle` ADD CONSTRAINT `fk_detalle_producto` FOREIGN KEY (`idProducto`)
REFERENCES `producto` (`idProducto`),
ADD CONSTRAINT `fk_detalle_transaccion` FOREIGN KEY (`idTransaccion`) 
REFERENCES `transaccion` (`idTransaccion`); 
COMMIT;
describe detalle; 
describe producto;
describe transaccion;
select*from producto;
select*from detalle;
select*from transaccion;
INSERT INTO `mitiendag12o24`.`producto` (`idProducto`, `nombreProducto`, `valorCompra`, `valorVenta`, `cantidadProducto`, `fechaVencimiento`) VALUES ('1', 'Panela', '2000', '2400', '100', '2022-01-15');
INSERT INTO `mitiendag12o24`.`producto` (`idProducto`, `nombreProducto`, `valorCompra`, `valorVenta`, `cantidadProducto`, `fechaVencimiento`) VALUES ('2', 'Arroz Diana', '1450', '1600', '120', '2022-02-07');
INSERT INTO mitiendag12o24.producto (idProducto,nombreProducto,valorCompra,valorVenta,cantidadProducto,fechaVencimiento) VALUES ('3', 'Cocacola 350 ml', '2350', '2700', '75', '2022-10-13');

#______________________________________________________________
INSERT INTO `mitiendag12o24`.`transaccion` (`idTransaccion`, `tipoTransaccion`, `fechaTransaccion`, `vendedor`, `comprador`, `total`) VALUES ('1', 'Venta', '2022-01-07', 'Alirio Ferreira', 'Vila Mejía', '3200');
INSERT INTO `mitiendag12o24`.`transaccion` (`idTransaccion`, `tipoTransaccion`, `fechaTransaccion`, `vendedor`, `comprador`, `total`) VALUES ('2', 'Compra', '2022-03-03', 'David Mejía', 'Paula Rojas', '23500');
INSERT INTO `mitiendag12o24`.`transaccion` (`idTransaccion`, `tipoTransaccion`, `fechaTransaccion`, `vendedor`, `comprador`, `total`) VALUES ('3', 'Venta', '2022-07-07', 'Alirio Garcia', 'Martha Velandia', '3200');

#________________________________________________________________
INSERT INTO `mitiendag12o24`.`detalle` (`idDetalle`, `idProducto`, `idTransaccion`, `valorDetalle`, `cantidadDetalle`, `totalDetalle`) VALUES ('1', '3', '2', '2350', '10', '23500');
INSERT INTO `mitiendag12o24`.`detalle` (`idDetalle`, `idProducto`, `idTransaccion`, `valorDetalle`, `cantidadDetalle`, `totalDetalle`) VALUES ('2', '1', '3', '2000', '7', '14000');
INSERT INTO `mitiendag12o24`.`detalle` (`idDetalle`, `idProducto`, `idTransaccion`, `valorDetalle`, `cantidadDetalle`, `totalDetalle`) VALUES ('3', '2', '1', '1600', '2', '3200');

#________________________________________________________________
CREATE TABLE `usuario` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cedula` varchar(20) COLLATE utf8mb4_general_ci NOT NULL,
  `usuario` varchar(25) COLLATE utf8mb4_general_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `clave` varchar(25) COLLATE utf8mb4_general_ci NOT NULL,
  `celular` varchar(16) COLLATE utf8mb4_general_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `activo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

INSERT INTO `usuario` VALUES (1,'91250421','padafe65','Alirio','1234','3041258741','padafe@gmail.com',1),(2,'32163511','vimesa','vilma','3213','314296574','vimesa76@hotmail.com',1);
select*from usuario;