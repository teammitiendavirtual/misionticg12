/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.latiendag12.Dao;

import com.example.latiendag12.Model.Transaccion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author padaf
 */
@Repository
public interface TransaccionDao extends CrudRepository<Transaccion,Integer> {
    
}
